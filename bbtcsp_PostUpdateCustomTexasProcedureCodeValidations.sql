IF OBJECT_ID('dbo.bbtcsp_PostUpdateCustomTexasProcedureCodeValidations') IS NOT NULL
	DROP PROCEDURE dbo.bbtcsp_PostUpdateCustomTexasProcedureCodeValidations
GO


CREATE PROCEDURE bbtcsp_PostUpdateCustomTexasProcedureCodeValidations
  @ScreenKeyId INT,                  
	@StaffId INT,                  
	@CurrentUser VARCHAR(30),                  
	@CustomParameters XML   
AS
/**************************************************************************
Created By: Andre Lai
Created Date: 2020-03-19
Purpose: to update procedure code id based on incoming procedure code name 
          for Custom Texas Procedure Code Validations Detail Screen

Modified Date     Modified By           Description

***************************************************************************/


BEGIN

DECLARE @today DATETIME = GETDATE()

BEGIN try
  BEGIN TRAN
    UPDATE ctpcv
    SET ProcedureCodeId = pc.ProcedureCodeId
    FROM CustomTexasProcedureCodeValidations AS ctpcv
    JOIN PRocedureCodes AS pc ON ctpcv.ProcedureCodeName =  pc.ProcedureCodeName
    WHERE @ScreenKeyId = ctpcv.TexasProviderValidationId
    
  COMMIT tran
END TRY

BEGIN catch
  IF @@TRANCOUNT > 0
            BEGIN
                ROLLBACK TRAN;
            END;
    
        DECLARE @errorMessage VARCHAR(MAX);
        SELECT
                @errorMessage = ERROR_MESSAGE() + CHAR(10) + CHAR(13) + ERROR_PROCEDURE();
        DECLARE @currentDate DATETIME = GETDATE();
	          
        EXEC dbo.ssp_SCLogError @ErrorMessage = @errorMessage , -- text
            @VerboseInfo = 'bbtcsp_PostUpdateCustomTexasProcedureCodeValidations'  , -- text
            @ErrorType = 'Severe' , -- varchar(50)
            @CreatedBy = @CurrentUser , -- varchar(30)
            @CreatedDate = @today , -- datetime
            @DatasetInfo = ''; -- text

RAISERROR (@errorMessage, 16, 1);

END catch





END
go


UPDATE Screens
SET PostUpdateStoredProcedure = 'bbtcsp_PostUpdateCustomTexasProcedureCodeValidations'
 FROM Screens WHERE ScreenName = 'Custom Texas Procedure Code Validations Details'