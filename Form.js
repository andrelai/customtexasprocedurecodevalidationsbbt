function ValidateCustomPageEventHandler()
{
  if ($("[id$=DropDownList_CustomTexasProcedureCodeValidations_ProcedureCodeName]").val() == "" || $("[id$=DropDownList_CustomTexasProcedureCodeValidations_ProcedureCodeName]").val() == null)
  {
    ShowHideErrorMessage("Procedure Code is required", "true");
    return false;
  }
  else if ($("[id$=DropDownList_CustomTexasProcedureCodeValidations_CategoryName]").val() == "" || $("[id$=DropDownList_CustomTexasProcedureCodeValidations_CategoryName]").val() == null)
  {
    ShowHideErrorMessage("Custom Field Type is required", "true");
    return false;
  }
  
  else if ($("[id$=DropDownList_CustomTexasProcedureCodeValidations_CodeName]").val() == "" || $("[id$=DropDownList_CustomTexasProcedureCodeValidations_CodeName]").val() == null)
  {
    ShowHideErrorMessage("Allowed Option is required", "true");
    return false;
  }

  else if ($("input[name='RadioButton_CustomTexasProcedureCodeValidations_DefaultValues']:checked").val() == "" || $("input[name='RadioButton_CustomTexasProcedureCodeValidations_DefaultValues']:checked").val() == null) 
  {
    ShowHideErrorMessage("Default Value is required", "true");
    return false;
  }

  else if ($("input[name='RadioButton_CustomTexasProcedureCodeValidations_AllowComplete']:checked").val() == "" || $("input[name='RadioButton_CustomTexasProcedureCodeValidations_AllowComplete']:checked").val() == null)
  {
    ShowHideErrorMessage("Allow services with Procedure-Custom field option to complete is required", "true");
    return false;
  }
  else {
    return true;
  }

}
