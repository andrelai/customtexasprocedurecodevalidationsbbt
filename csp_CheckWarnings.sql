IF OBJECT_ID('dbo.csp_CheckWarnings') IS NOT NULL
  DROP PROCEDURE dbo.csp_CheckWarnings
GO

CREATE PROCEDURE [dbo].[csp_CheckWarnings]
/* Param List */
@ClientId INT,
@ServiceId INT,
@ProcedureCodeId INT,
@ClinicianId INT,
@StartDate DATETIME,
@EndDate DATETIME,
@Attending VARCHAR(10),
@DSMCode1 VARCHAR(10),
@DSMCode2 VARCHAR(10),
@DSMCode3 VARCHAR(10),
@ServiceCompletionStatus VARCHAR(10),
@ProgramId INT,
@LocationId INT,
@Degree INT,
@UnitValue DECIMAL(9, 2),
@Count INT,
@ServiceAlreadyCompleted CHAR(1),
@Billable CHAR(1),
@DoesNotRequireStaffForService CHAR(1),
@PreviousStatus INT
/******************************************************************************
**
**		Name: csp_CheckWarnings
**		Desc: 
**		This procedure is used to check the warnings for the service, while completing the service.
**
**		Return values:
** 
**		Called by:   ServiceDetails.cs
**              
**		Parameters:
**		Input							Output
**              @ClientId int,
**		@ServiceId int,
**		@ProcedureCodeId int,
**		@ClinicianId int,
**		@StartDate DateTime,
**		@EndDate DateTime,
**		@Attending varchar(10),
**		@DSMCode1 varchar(10),
**		@DSMCode2 varchar(10),
**		@DSMCode3 varchar(10)						
**
**		Auth: Rohit
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:	 	  Author:   Description:
		05.18.2016	  Shaik.Irfan	Added Logic to create a service completion rule for Procedure code HOSPITAL AFTERCARE> require modifier to complete the service for Camino - Customizations - Task#71	
		24/May/2016	  SuryaBalan	Created this service completion rule = If client has plan of ECI Amerigroup then service requires a Referring do not let service complete without selection of Referring 
		                            Camino - Environment Issues Tracking #171
		25/May/2016	  SuryaBalan	Created 24/May/2016	  SuryaBalan Created this service completion rule = If client has plan of ECI Amerigroup then service requires a
																		 Referring do not let service complete without selection of Referring/Camino - Environment Issues Tracking #94 
		                           
		25/May/2016	 Neelima	Created to insert warning when no Program in TRR-Track Camino - Environment Issues Tracking #87	
		14/Jun/2016  Shaik.Irfan    Added the status parameter line to show the Warning/Errors on Show and Complete
									For Camino - Environment Issues Tracking - Task#170
	  06/21/2016	    jcarlson added AorB validation, removed old non Camino validations
	  07/15/2016	    jcarlson	 added logic to require add on codes for procedure codes in "xProcedureCodesRequireAddOn"
	  08/24/2016	    jcarlson	 added logic to require an attending for procedure codes in "xProcedureCodesRequireAttending"
	  10/25/2016        Pabitra      Changed the logic to check if the client is enrolled in a TRR Track and show the Service Warning.
                                	  Why:CSGL#177
       10/27/2016	    jcarlson	 Camino Support Go Live - 198 : Texas Transportation Validation
	  11/21/2016	    jcarlson	 moved all common texas validations to the scsp TxAce Environment Issues Tracking 117
    3/19/2020       alai      added custom logic for bluebonnet to prevent services from going to complete if a) a procedure code validation exists for the Custom Field
                  b) the selected option from the provider does not allow completion based on CustomTexasProcedureCodeValidations.AllowComplete
*******************************************************************************/
AS

  DECLARE @ModeOfDelivery VARCHAR(250)
  DECLARE @CrisisType VARCHAR(250)
  DECLARE @Recipient VARCHAR(250)

  DECLARE @ModeOfDeliveryExistsInCustomTexasProcedureCodeValidations CHAR(1) = 'N'
  DECLARE @CrisisTypeExistsInCustomTexasProcedureCodeValidations CHAR(1) = 'N'
  DECLARE @RecipientExistsInCustomTexasProcedureCodeValidations CHAR(1) = 'N'

  DECLARE @ModeOfDeliveryErrorType varchar(250)
  DECLARE @CrisisTypeErrorType varchar(250)
  DECLARE @RecipientsErrorType varchar(250)
  
  DECLARE @ModeOfDeliveryErrorTypeGlobalCodeId int
  DECLARE @CrisisTypeErrorTypeGlobalCodeId int
  DECLARE @RecipientsErrorTypeGlobalCodeId int

  select top 1 
    @ModeOfDeliveryErrorTypeGlobalCodeId = GlobalCodeId
    , @ModeOfDeliveryErrorType = CodeName
  FROM
    dbo.GlobalCodes
  WHERE
    Category = 'SERVICEERRORTYPE'
    AND CodeName = 'Custom field - Mode of Delivery does not allow service completion'
    AND ISNULL(RecordDeleted, 'N') <> 'Y'
  order by 
    GlobalCodeId

  select top 1 
    @CrisisTypeErrorTypeGlobalCodeId = GlobalCodeId
    , @CrisisTypeErrorType = CodeName
  FROM
    dbo.GlobalCodes
  WHERE
    Category = 'SERVICEERRORTYPE'
    AND CodeName = 'Custom field - Crisis does not allow service completion'
    AND ISNULL(RecordDeleted, 'N') <> 'Y'
  order by 
    GlobalCodeId

  select top 1 
    @RecipientsErrorTypeGlobalCodeId = GlobalCodeId
    , @RecipientsErrorType = CodeName
  FROM
    dbo.GlobalCodes
  WHERE
    Category = 'SERVICEERRORTYPE'
    AND CodeName = 'Custom field - Recipients does not allow service completion'
    AND ISNULL(RecordDeleted, 'N') <> 'Y'
  order by 
    GlobalCodeId

  SELECT
    @ModeOfDelivery = dbo.csf_GetGlobalCodeNameById(cs.ModeOfDelivery)
   ,@CrisisType = dbo.csf_GetGlobalCodeNameById(cs.Crisis)
   ,@Recipient = dbo.csf_GetGlobalCodeNameById(cs.Recipient)
  FROM
    CustomServices AS cs
  WHERE
    cs.ServiceId = @ServiceId
    AND ISNULL(cs.RecordDeleted, 'N') = 'N'


  -- procedure code validations exist for this procedure code and category
  IF EXISTS (SELECT
        1
      FROM CustomTexasProcedureCodeValidations AS ctpcv
      WHERE @ProcedureCodeId = ctpcv.ProcedureCodeId
      AND ISNULL(ctpcv.RecordDeleted, 'N') = 'N'
      AND ctpcv.CategoryName = 'xModeOfDelivery')
    BEGIN
      SET @ModeOfDeliveryExistsInCustomTexasProcedureCodeValidations = 'Y'
    END

  IF EXISTS (SELECT
        1
      FROM CustomTexasProcedureCodeValidations AS ctpcv
      WHERE @ProcedureCodeId = ctpcv.ProcedureCodeId
      AND ISNULL(ctpcv.RecordDeleted, 'N') = 'N'
      AND ctpcv.CategoryName = 'xCrisisType')
     BEGIN
      SET @CrisisTypeExistsInCustomTexasProcedureCodeValidations = 'Y'
    END
 
  IF EXISTS (SELECT
        1
      FROM CustomTexasProcedureCodeValidations AS ctpcv
      WHERE @ProcedureCodeId = ctpcv.ProcedureCodeId
      AND ISNULL(ctpcv.RecordDeleted, 'N') = 'N'
      AND ctpcv.CategoryName = 'xServiceRecipients')
    BEGIN
      SET @RecipientExistsInCustomTexasProcedureCodeValidations = 'Y'
    END


  -- mode of delivery
  IF (@ModeOfDeliveryExistsInCustomTexasProcedureCodeValidations = 'Y')
    begin
      -- mode of delivery NOT entered
      IF (@ModeOfDelivery is null)
        BEGIN
            SELECT
              @ServiceId
              ,@ModeOfDeliveryErrorTypeGlobalCodeId
              ,@ModeOfDeliveryErrorType +  ': "Mode of delivery" is missing'
        END
      -- mod of delivery entered AND mode of delivery selected does not allow complete
      ELSE IF EXISTS (SELECT
                1
              FROM CustomTexasProcedureCodeValidations AS ctpcv
              WHERE @ProcedureCodeId = ctpcv.ProcedureCodeId
              AND ISNULL(ctpcv.RecordDeleted, 'N') = 'N'
              AND ISNULL(ctpcv.AllowComplete, 'N') = 'N'
              AND ctpcv.CategoryName = 'xModeOfDelivery'
              AND ctpcv.CodeName = @ModeOfDelivery)
        BEGIN
          INSERT INTO dbo.ServiceErrors (
            ServiceId
          ,ErrorType
          ,ErrorMessage)
          SELECT
            @ServiceId
            ,@ModeOfDeliveryErrorTypeGlobalCodeId
            ,REPLACE(@ModeOfDeliveryErrorType, 'does not allow service completion', ': "' + @ModeOfDelivery + '" does not allow service completion')
        END
    END
 

  -- crisis type
  IF (@CrisisTypeExistsInCustomTexasProcedureCodeValidations = 'Y')
    BEGIN
    -- crisis type NOT entered
      IF (@CrisisType is null)
        BEGIN
            SELECT
              @ServiceId
              ,@CrisisTypeErrorTypeGlobalCodeId
              ,@CrisisTypeErrorType +  ': "Crisis" is missing'
        END
      -- crisis type exists AND crisis type selected does not allow complete
      else IF EXISTS (SELECT
            1
          FROM CustomTexasProcedureCodeValidations AS ctpcv
          WHERE @ProcedureCodeId = ctpcv.ProcedureCodeId
          AND ISNULL(ctpcv.RecordDeleted, 'N') = 'N'
          AND ISNULL(ctpcv.AllowComplete, 'N') = 'N'
          AND ctpcv.CategoryName = 'xCrisisType'
          AND ctpcv.CodeName = @CrisisType)
      BEGIN
        INSERT INTO dbo.ServiceErrors (
          ServiceId
        ,ErrorType
        ,ErrorMessage)
        SELECT
          @ServiceId -- ServiceId - int
          ,@CrisisTypeErrorTypeGlobalCodeId
          ,REPLACE(@CrisisTypeErrorType, 'does not allow service completion', ': "' + @CrisisType + '" does not allow service completion')
      END
    END
  
  
  -- recipients
  IF (@RecipientExistsInCustomTexasProcedureCodeValidations = 'Y')
    BEGIN
      -- recipients NOT entered
      IF (@Recipient is null)
        BEGIN
            SELECT
              @ServiceId
              ,@RecipientsErrorTypeGlobalCodeId
              ,@RecipientsErrorType +  ': "Recipients" is missing'
        END
      -- recipient exists AND recipient selected does not allow complete
      else IF EXISTS (SELECT
            1
          FROM CustomTexasProcedureCodeValidations AS ctpcv
          WHERE @ProcedureCodeId = ctpcv.ProcedureCodeId
          AND ISNULL(ctpcv.RecordDeleted, 'N') = 'N'
          AND ISNULL(ctpcv.AllowComplete, 'N') = 'N'
          AND ctpcv.CategoryName = 'xServiceRecipients'
          AND ctpcv.CodeName = isnull(@Recipient, ''))
        BEGIN
          INSERT INTO dbo.ServiceErrors (
            ServiceId
          ,ErrorType
          ,ErrorMessage)
          SELECT
            @ServiceId
            ,@RecipientsErrorTypeGlobalCodeId
            ,REPLACE(@RecipientsErrorType, 'does not allow service completion', ': "' + @Recipient + '" does not allow service completion')
        END
    END


  RETURN;
GO