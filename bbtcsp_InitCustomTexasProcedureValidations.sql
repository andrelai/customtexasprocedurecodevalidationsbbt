CREATE PROCEDURE bbtcsp_InitCustomTexasProcedureValidations
  	@ClientId int,                        
	  @StaffID int,                      
	  @CustomParameters xml
AS
BEGIN

SELECT 
  'CustomTexasProcedureCodeValidations' AS TableName
  ,-1 AS TexasProviderValidationId
  ,current_user as CreatedBy
	,getdate() as CreatedDate
	,current_user as ModifiedBy
	,getdate() as ModifiedDate


  --Checking For Errors           
If (@@error!=0)                
Begin                           
RAISERROR ('bbtcsp_InitCustomTexasProcedureValidations : An Error Occured',16,1)               
Return                         
End


END

