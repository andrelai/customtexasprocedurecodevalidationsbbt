
BEGIN TRAN


SELECT *
FROM CustomTexasProcedureCodeValidations
UPDATE CustomTexasProcedureCodeValidations
SET CategoryName = CASE CategoryName WHEN 'xServiceRecipients' THEN 'XSERVICERECIPIENTS'
                                       WHEN 'xModeOfDelivery' THEN 'XMODEOFDELIVERY'
                                       WHEN 'xCrisisType' THEN 'XCRISISTYPE'
                      END

SELECT *
FROM CustomTexasProcedureCodeValidations

commit