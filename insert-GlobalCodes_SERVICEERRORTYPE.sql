
IF NOT EXISTS (
  SELECT *
  FROM GlobalCodes AS gc
  WHERE gc.Category = 'SERVICEERRORTYPE'
  AND ISNULL(gc.RecordDeleted, 'N') = 'N'
  AND gc.CodeName = 'Custom field - Mode of Delivery does not allow service completion'
)
BEGIN
  INSERT INTO GlobalCodes (
   Category
   ,CodeName
   ,Description
   ,Active
   ,CannotModifyNameOrDelete)
  SELECT
    'SERVICEERRORTYPE'
    ,'Custom field - Mode of Delivery does not allow service completion'
    ,'Procedure in Procedure Code Validation does not allow completion of service'
    ,'Y'
    ,'Y'

END 

IF NOT EXISTS (
  SELECT *
  FROM GlobalCodes AS gc
  WHERE gc.Category = 'SERVICEERRORTYPE'
  AND ISNULL(gc.RecordDeleted, 'N') = 'N'
  AND gc.CodeName = 'Custom field - Crisis does not allow service completion'
)
BEGIN
  INSERT INTO GlobalCodes (
   Category
   ,CodeName
   ,Description
   ,Active
   ,CannotModifyNameOrDelete)
  SELECT
    'SERVICEERRORTYPE'
    ,'Custom field - Crisis does not allow service completion'
    ,'Procedure in Procedure Code Validation does not allow completion of service'
    ,'Y'
    ,'Y'
END



IF NOT EXISTS (
  SELECT *
  FROM GlobalCodes AS gc
  WHERE gc.Category = 'SERVICEERRORTYPE'
  AND ISNULL(gc.RecordDeleted, 'N') = 'N'
  AND gc.CodeName = 'Custom field - Recipients does not allow service completion'
)
BEGIN
  INSERT INTO GlobalCodes (
   Category
   ,CodeName
   ,Description
   ,Active
   ,CannotModifyNameOrDelete)
  SELECT
    'SERVICEERRORTYPE'
    ,'Custom field - Recipients does not allow service completion'
    ,'Procedure in Procedure Code Validation does not allow completion of service'
    ,'Y'
    ,'Y'
end