IF OBJECT_ID('dbo.bbtcsp_GetCustomFieldsCodeName') IS NOT NULL
	DROP PROCEDURE dbo.bbtcsp_GetCustomFieldsCodeName
GO


CREATE PROCEDURE [dbo].[bbtcsp_GetCustomFieldsCodeName]
AS 

BEGIN


SELECT CategoryName = CASE gc.Category WHEN 'xServiceRecipients' THEN 'Service Recipients'
                                       WHEN 'xModeOfDelivery' THEN 'Mode of Delivery'
                                       WHEN 'xCrisisType' THEN 'Crisis' 
                    END + ' - ' + gc.CodeName
      ,gc.CodeName                                       
FROM GlobalCodes AS gc WHERE gc.Category IN ( 'XCrisisType', 'XModeOfDelivery', 'XServiceRecipients') AND ISNULL(gc.RecordDeleted, 'N')=  'N' AND gc.Active = 'Y'

end
GO


